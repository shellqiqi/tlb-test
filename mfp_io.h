#ifndef MFP_MEMORY_MAPPED_REGISTERS_H
#define MFP_MEMORY_MAPPED_REGISTERS_H

#define MFP_LEDS_ADDR           0xB0600000
#define MFP_SWITCHES_ADDR       0xB0C00000
#define SEVEN_SEG_EN_ADDR       0xB0800000
#define SEVEN_SEG_ADDR          0xB0810000

#define MFP_LEDS                (* (volatile unsigned *) MFP_LEDS_ADDR      )
#define MFP_SWITCHES            (* (volatile unsigned *) MFP_SWITCHES_ADDR      )
#define MFP_SEVEN_SEG_EN        (* (volatile unsigned *) SEVEN_SEG_EN_ADDR       )
#define MFP_SEVEN_SEG           (* (volatile unsigned *) SEVEN_SEG_ADDR      )

// This define is used in boot.S code

#define BOARD_16_LEDS_ADDR      MFP_LEDS_ADDR

#endif
